//
//  MGYelpSearchHelper.h
//  MGYelpHelper
//
//  Created by Michael Graziano on 12/29/14.
//  Copyright (c) 2014 Mike Graziano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Business.h"

@interface MGYelpSearchHelper : NSObject

/**
 Perform a search using the Yelp API. This is the primary interface for the Objective-C wrapper.
 
 @param params A dictionary of parameters to pass into the search handler. Created most easily with the NSMutableDictionary+YelpParameters category
 @param completionBlock The block of code that will handle the returned array of search result data
 
 @return returnArray The completion block is given an array comtaining Business objects that were returned fron the search request
 @return error In the case of an error, the error is passed through to the completion block. Nil on success.
 */
+ (void)fetchSearchResultsForParameters:(NSDictionary *)params onCompletion:(void (^)(NSArray *returnArray, NSError *error))completionBlock;

+ (void)fetchBusinessResultForParameters:(NSDictionary *)params onCompletion:(void (^)(Business *business, NSError *error))completionBlock;


@end
