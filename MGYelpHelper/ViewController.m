//
//  ViewController.m
//  MGYelpHelper
//
//  Created by Michael Graziano on 12/29/14.
//  Copyright (c) 2014 Mike Graziano. All rights reserved.
//

#import "ViewController.h"

#import "NSMutableDictionary+YelpParameters.h"
#import "MGYelpSearchHelper.h"

@interface ViewController ()

@property (strong, nonatomic) NSArray *searchResultsArray;
@property (strong, nonatomic) Business *business;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self sendSearchRequestWithParameters:[self parametersRequest]];
    [self sendBusinessRequestWithParameters:[self businessParams]];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableDictionary *)parametersRequest
{
    NSMutableDictionary *paramsdict = [[NSMutableDictionary alloc] init];
    
    [paramsdict setLocationWithCity:@"Minneapolis,MN"];
    [paramsdict setSearchResultsLimit:@10];
    
    return paramsdict;
}

- (NSDictionary *)businessParams
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setBusinessId:@"112-eatery-minneapolis"];
    return params;
}

- (void)sendSearchRequestWithParameters:(NSMutableDictionary *)params
{
    [MGYelpSearchHelper fetchSearchResultsForParameters:params onCompletion:^(NSArray *returnArray, NSError *error) {
        self.searchResultsArray = returnArray;
    }];
}

- (void)sendBusinessRequestWithParameters:(NSDictionary *)params
{
    [MGYelpSearchHelper fetchBusinessResultForParameters:params onCompletion:^(Business *business, NSError *error) {
        self.business = business;
    }];
}

@end
