//
//  MGYelpSearchHelper.m
//  MGYelpHelper
//
//  Created by Michael Graziano on 12/29/14.
//  Copyright (c) 2014 Mike Graziano. All rights reserved.
//

#import "MGYelpSearchHelper.h"
#import <TDOAuth/TDOAuth.h>
#import "Business.h"

/* Constants for accessing the Yelp API. Provides host address and paths to search and business APIs */
static NSString * const kAPIHost           = @"api.yelp.com";
static NSString * const kSearchPath        = @"/v2/search";
static NSString * const kBusinessPath      = @"/v2/business/";

/* Enter your Yelp API credentials here */
static NSString * const kConsumerKey       = @"Rht71QVt8rKiuTtpUnSIXA";
static NSString * const kConsumerSecret    = @"sG3WK5k-sdLDlFAtDc5f1K5K8hI";
static NSString * const kAccessToken       = @"x9SwNq8aS-rbmZ1o46YhhYTp7UlqneGb";
static NSString * const kTokenSecret       = @"yhkKJczD3zZfmmFPvrqerAL0qNE";

@implementation MGYelpSearchHelper

#pragma mark - Public methods

+ (void)fetchSearchResultsForParameters:(NSDictionary *)params onCompletion:(void (^)(NSArray* returnArray, NSError *error))completionBlock
{
    NSURLRequest *request = [MGYelpSearchHelper searchRequestWithParams:params];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        if (!error && httpResponse.statusCode == 200) {
            completionBlock([self generateResultsArrayWithDict:[NSJSONSerialization JSONObjectWithData:data options:0 error:&error]], nil);
        } else {
            completionBlock(nil, error);
        }
    }] resume];
}

+ (void)fetchBusinessResultForParameters:(NSDictionary *)params onCompletion:(void (^)(Business *business, NSError *error))completionBlock
{
    NSURLRequest *request = [MGYelpSearchHelper businessRequestWithParams:params];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        if (!error && httpResponse.statusCode == 200) {
            completionBlock(nil, nil);
        } else {
            completionBlock(nil, nil);
        }
    }] resume];
}


#pragma mark - Private mathods
/*
 Creates an NSURLRequest with the provided OAuth information. Takes in the parameters dictionary to build the request as well as if the request is utilizing the search API or the business API
 */
+ (NSURLRequest *)searchRequestWithParams:(NSDictionary *)params
{
    return [TDOAuth URLRequestForPath:kSearchPath
                        GETParameters:params
                                 host:kAPIHost
                          consumerKey:kConsumerKey
                       consumerSecret:kConsumerSecret
                          accessToken:kAccessToken
                          tokenSecret:kTokenSecret
            ];
}

+ (NSURLRequest *)businessRequestWithParams:(NSDictionary *)params
{
    NSMutableDictionary *paramsDict = [params mutableCopy];
    NSString *businessId = [paramsDict objectForKey:@"id"];
    [paramsDict removeObjectForKey:@"id"];
    NSString *urlPath = [kBusinessPath stringByAppendingString:businessId];
    return [TDOAuth URLRequestForPath:urlPath
                        GETParameters:nil
                                 host:kAPIHost
                          consumerKey:kConsumerKey
                       consumerSecret:kConsumerSecret
                          accessToken:kAccessToken
                          tokenSecret:kTokenSecret];
}

/*
 Takes a dictionary generated from the response JSON, and returns an ordered array of search results
 */
+ (NSMutableArray *)generateResultsArrayWithDict:(NSDictionary *)searchResponseJSON
{
    NSArray *rawBusinessesArray = searchResponseJSON[@"businesses"];
    NSMutableArray *businessArrayToReturn = [[NSMutableArray alloc] initWithCapacity:rawBusinessesArray.count];
    
    for (NSDictionary *businessData in rawBusinessesArray) {
        Business *businessFromJSON = [[Business alloc] initWithJSONDictionary:businessData];
        [businessArrayToReturn addObject:businessFromJSON];
    }
    return businessArrayToReturn;
}

+ (Business *)businessFromJSONResponseDict:(NSDictionary *)searchResponseJSON
{
    
    return [[Business alloc] initWithJSONDictionary:searchResponseJSON];
}



@end
