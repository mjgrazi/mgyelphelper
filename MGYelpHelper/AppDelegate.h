//
//  AppDelegate.h
//  MGYelpHelper
//
//  Created by Michael Graziano on 12/29/14.
//  Copyright (c) 2014 Mike Graziano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

